package com.HttpConnections;

import javax.json.JsonNumber;

/**
 * Created by Sebitas on 7/6/17.
 */
public class Location {

    private double lat;
    private double lng;

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "Lat: "+this.lat +" Lng: "+this.lng;
    }
}

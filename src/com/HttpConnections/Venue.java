package com.HttpConnections;

/**
 * Created by Sebitas on 7/6/17.
 */
public class Venue {

    private String id;
    private String name;
    private double distance;
    private Location location;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getDistance() {
        return distance;
    }

    public Location getLocation() {
        return location;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Name: "+ this.name + " Location: " + this.location;
    }
}

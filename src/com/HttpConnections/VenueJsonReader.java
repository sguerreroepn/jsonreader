package com.HttpConnections;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

/**
 * Created by Sebitas on 7/6/17.
 */
public class VenueJsonReader {

    //public static final String JSON_FILE= "/venues.json";

    public void readJSONFile() throws IOException {

        InputStream fis = new FileInputStream("/Users/Sebitas/Documents/EPN/JAVA/OCJP/venues.json");

        //create JsonReader object
        JsonReader jsonReader = Json.createReader(fis);

        //get JsonObject from JsonReader
        JsonObject jsonObject = jsonReader.readObject();

        //we can close IO resource and JsonReader now
        jsonReader.close();
        fis.close();

        JsonObject response = jsonObject.getJsonObject("response");
        JsonArray venueArray = response.getJsonArray("venues");

        ArrayList<Venue> venues = new ArrayList<Venue>();


        for(JsonValue jvalue : venueArray){

            JsonObject jo = (JsonObject) jvalue;

            Venue v = new Venue();

            v.setName(jo.getString("name"));
            v.setId(jo.getString("id"));

            Location location = new Location();

            JsonObject loc = jo.getJsonObject("location");

            location.setLat(loc.getJsonNumber("lat").doubleValue());
            location.setLng(loc.getJsonNumber("lng").doubleValue());

            v.setLocation(location);
            v.setDistance(loc.getJsonNumber("distance").doubleValue());

            venues.add(v);

        }

        for(Venue v: venues){
            System.out.println(v.toString());
        }

    }


}

package com.company;

import com.HttpConnections.VenueJsonReader;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
	// write your code here

        VenueJsonReader vjr = new VenueJsonReader();

        try {
            vjr.readJSONFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}